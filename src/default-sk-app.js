
import { SkAppImpl }  from '../../sk-app/src/impl/sk-app-impl.js';

export class DefaultSkApp extends SkAppImpl {

    get prefix() {
        return 'default';
    }

    get suffix() {
        return 'app';
    }

}
